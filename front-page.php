<?php
get_header();

?>
<section>
<picture>
    <source srcset="<?php echo get_template_directory_uri(); ?>/img/fokep.jpg" media="(min-width: 800px)">
    <img src="<?php echo get_template_directory_uri(); ?>/img/fokep_m.jpg" alt="fokep">
</picture>
</section>

<?php

get_footer();


